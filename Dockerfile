FROM squidfunk/mkdocs-material:latest

COPY requirements.txt /tmp/requirements.txt

# Install mkdocs-material plugins
RUN pip install --no-cache-dir -r /tmp/requirements.txt

# Copy the project documentation into the container
COPY . /docs

WORKDIR /docs